# Welcome to 2coders App!

*Welcome to my technical test that I have developed for the company "2coders" located in Gran Canaria.*

## Clone the repository in a local folder.

`git clone git@gitlab.com:MajdiKB/2coders.git`

## Back, database.

The database was developed with Laravel, then install dependencies. 

In the folder: 

*back/2coders/*

execute:

`npm i`

`composer i`

#### Create your mysql database

In your terminal execute:

`mysql -u root -p`

and create a database:

`create database 2coders;`

#### Create your own .env

In the folder:

*back/2coders/*

execute:

`cp .env.example .env`

and generate your Laravel Key...

`php artisan key:generate`

## IMPORTANT!!!

_In your file .env you have to config:_

`DB_DATABASE=2coders`

`DB_USERNAME=root`

`DB_PASSWORD=YOUR_ROOT_PASS_IF_YOU_HAVE_PASSWORD`

#### Config tables in your database with migrate.

In the folder:  

*back/2coders/*

execute:

`php artisan migrate`

#### Now run db with artisan.

In the same folder (*back/2coders/*), execute: 

`php artisan serve`

*Now server is working in localhost:8000.*

#### Optional-Tests:

If you want to try the tests, in the same folder (*back/2coders/*), execute:

`php artisan test`

## Front, Angular cli.

If you dont have installed Angular cli, execute:

`npm install -g @angular/cli`

After that, in the folder: 

*front/twocoders/*

execute:

`npm i`

#### Now run the project.

`ng serve --o`

*Now the project is working in localhost:4200.*

***App created by Majdi Kokaly***