import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FruitModel } from '../models/fruit-model';
import { Services } from '../../services/services.service';

@Component({
  selector: 'app-fruit-details',
  templateUrl: './fruit-details.component.html',
  styleUrls: ['./fruit-details.component.scss']
})
export class FruitDetailsComponent implements OnInit {
  public fruit: FruitModel = {} as FruitModel;
  public fruitId: string = "";
  public confirmDelete: boolean = false;

  constructor(private route: ActivatedRoute, private fruitsServices: Services, private router: Router) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe((params) => {
      this.fruitId = params.get('id') as string;
    })
    this.getFruitDetails(this.fruitId);
  }

  public getFruitDetails(fruitId: string): void {
    this.fruitsServices.getFruitDetails(fruitId).subscribe((data: FruitModel) => {
      this.fruit = data;
    })
  }

  public deleteFruit(fruitId: string):void{
    this.fruitsServices.deleteFruit(fruitId).subscribe();
    this.confirmDelete=false;
    setTimeout(() => {
      this.router.navigate(['/fruits/']);
    }, 100);
  }
}
