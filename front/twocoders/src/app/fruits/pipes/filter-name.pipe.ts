import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterName'
})
export class FilterNamePipe implements PipeTransform {

  transform(list: any[], fruitFilterInput: string = '', key: string): any[] {

    const filteredList: any[] = list.filter((el: any) => {
      // el include devuelve un booleano, q si encuentra algo parecido lo muestra
      const match: boolean = el[key].includes(fruitFilterInput);
      return match;
    });
    return filteredList;
  }
}