import { Component, OnInit } from '@angular/core';
import { Services } from '../../services/services.service';
import { FruitModel } from '../models/fruit-model';

@Component({
  selector: 'app-fruits-list',
  templateUrl: './fruits-list.component.html',
  styleUrls: ['./fruits-list.component.scss']
})
export class FruitsListComponent implements OnInit {
  public fruitsList: FruitModel[] = [];
  public filterName: string = "";

  constructor(private fruitsService: Services) { }

  ngOnInit(): void {
    this.getFruitsList();
  }

  public getFruitsList():void{
    this.fruitsService.getFruitsList().subscribe(
      (data: FruitModel[]) => {
        const result: FruitModel[] = data;
        this.fruitsList = result;
      }
    )
  }
}
