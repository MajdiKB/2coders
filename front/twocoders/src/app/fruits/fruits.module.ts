import { FruitsListComponent } from './fruits-list/fruits-list.component';
import { Services } from '../services/services.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FruitsRoutingModule } from './fruits-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { FruitDetailsComponent } from './fruit-details/fruit-details.component';
import { FruitCreateFormComponent } from './fruit-create-form/fruit-create-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FruitUpdateFormComponent } from './fruit-update-form/fruit-update-form.component';
import { FilterNamePipe } from './pipes/filter-name.pipe';
import { NgSelectModule } from '@ng-select/ng-select';


@NgModule({
  declarations: [
    FruitsListComponent,
    FruitDetailsComponent,
    FruitCreateFormComponent,
    FruitUpdateFormComponent,
    FilterNamePipe
  ],
  imports: [
    CommonModule,
    FruitsRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    NgSelectModule
  ],
  exports: [
    HttpClientModule,
    ReactiveFormsModule,
  ],
  providers: [Services]
})
export class FruitsModule { }
