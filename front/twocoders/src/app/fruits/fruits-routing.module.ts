import { FruitUpdateFormComponent } from './fruit-update-form/fruit-update-form.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FruitCreateFormComponent } from './fruit-create-form/fruit-create-form.component';
import { FruitDetailsComponent } from './fruit-details/fruit-details.component';
import { FruitsListComponent } from './fruits-list/fruits-list.component';

const routes: Routes = [
  { path: '', component: FruitsListComponent },
  { path: 'new', component: FruitCreateFormComponent },
  { path: 'update/:id', component: FruitUpdateFormComponent },
  { path: 'details/:id', component: FruitDetailsComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FruitsRoutingModule { }
