import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FruitUpdateFormComponent } from './fruit-update-form.component';

describe('FruitUpdateFormComponent', () => {
  let component: FruitUpdateFormComponent;
  let fixture: ComponentFixture<FruitUpdateFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FruitUpdateFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FruitUpdateFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
