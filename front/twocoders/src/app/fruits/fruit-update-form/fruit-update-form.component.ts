import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Services } from 'src/app/services/services.service';
import { FruitModel } from '../models/fruit-model';

@Component({
  selector: 'app-fruit-update-form',
  templateUrl: './fruit-update-form.component.html',
  styleUrls: ['./fruit-update-form.component.scss'],
})
export class FruitUpdateFormComponent implements OnInit {
  public updateFruitForm: FormGroup = new FormGroup({});
  public submitted: boolean = false;
  public fruitId: string = '';
  public fruit: FruitModel = {} as FruitModel;

  constructor(
    private formBuilder: FormBuilder,
    private fruitServices: Services,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.route.paramMap.subscribe((params) => {
      this.fruitId = params.get('id') as string;
    });

    this.getDetailsFruit(this.fruitId);

    this.updateFruitForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.maxLength(30)]],
      size: ['', [Validators.required]],
      color: ['', [Validators.maxLength(30)]],
      photo: ['', [Validators.maxLength(100)]],
    });
  }

  public onSubmit(): void {
    this.submitted = true;
    if (this.updateFruitForm.valid) {
      const updateFruit: any = {
        name: this.updateFruitForm.get('name')?.value,
        size: this.updateFruitForm.get('size')?.value,
        color: this.updateFruitForm.get('color')?.value,
        photo: this.updateFruitForm.get('photo')?.value,
      };
      this.fruitServices
        .updateFruit(updateFruit, this.fruitId)
        .subscribe(() => {
          this.router.navigate(['/fruits/details/' + this.fruitId ]);
        });
      this.updateFruitForm.reset();
      this.submitted = false;
    }
  }
  public getDetailsFruit(fruitId: string): void {
    this.fruitServices.getFruitDetails(fruitId).subscribe((data) => {
      this.fruit = data;
    });
  }
}
