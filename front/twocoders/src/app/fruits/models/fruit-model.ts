export interface FruitModel{
  id:	string,
  name:	string,
  size:	string,
  color:	string,
  photo: string,
}