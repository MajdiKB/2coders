import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Services } from '../../services/services.service';

@Component({
  selector: 'app-fruit-create-form',
  templateUrl: './fruit-create-form.component.html',
  styleUrls: ['./fruit-create-form.component.scss'],
})
export class FruitCreateFormComponent implements OnInit {
  public newFruitForm: FormGroup = new FormGroup({});
  public submitted: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private fruitServices: Services,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.newFruitForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.maxLength(30)]],
      size: [null, [Validators.required]],
      color: ['', [Validators.maxLength(30)]],
      photo: ['', [Validators.maxLength(100)]],
    });
  }

  public onSubmit(): void {
    this.submitted = true;
    if (this.newFruitForm.valid) {
      const newFruit: any = {
        name: this.newFruitForm.get('name')?.value,
        size: this.newFruitForm.get('size')?.value,
        color: this.newFruitForm.get('color')?.value,
        photo: this.newFruitForm.get('photo')?.value,
      };
      this.fruitServices.addFruit(newFruit).subscribe(() => {
        this.router.navigate(['/fruits/']);
      })
      this.newFruitForm.reset();
      this.submitted=false;
    }
  }
}
