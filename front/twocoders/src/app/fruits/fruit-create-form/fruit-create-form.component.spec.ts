import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FruitCreateFormComponent } from './fruit-create-form.component';

describe('FruitCreateFormComponent', () => {
  let component: FruitCreateFormComponent;
  let fixture: ComponentFixture<FruitCreateFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FruitCreateFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FruitCreateFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
