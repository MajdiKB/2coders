import { CoreModule } from './core/core.module';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: `fruits`, loadChildren: () =>
      import('./fruits/fruits.module').then(m => m.FruitsModule)
  },
  {
    path: `error`, loadChildren: () =>
      import('./core/core.module').then(m => m.CoreModule)
  },
  { path: ``, redirectTo: `fruits`, pathMatch: `full` },
  { path: `**`, redirectTo: `fruits`, pathMatch: `full` },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
