import { Router } from '@angular/router';
import { environment } from './../../environments/environment.prod';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { FruitModel } from '../fruits/models/fruit-model';

@Injectable()
export class Services {
  public baseUrl: string = environment.base_url;

  constructor(private http: HttpClient, private router: Router) { }

  public getFruitsList():Observable<FruitModel[]>{
    return this.http.get<FruitModel[]>(this.baseUrl + "api/fruits").pipe(
      map((res:FruitModel[]) => {
        const results: FruitModel[] = res;
        return results;
      }),
      catchError(err => {
        const error = "No connection to DB";
        this.router.navigate(['/error/', {error: error}]);
        throw new Error(err.message);
      })
    );
  }

  public getFruitDetails(fruitId: string):Observable<FruitModel>{
    return this.http.get<FruitModel>(this.baseUrl + "api/fruit/" + fruitId).pipe(
      map((res:FruitModel) => {
        const results: FruitModel = res;
        return results;
      }),
      catchError(err => {
        const error = "No connection to DB";
        this.router.navigate(['/error/', {error: error}]);
        throw new Error(err.message);
      })
    );
  }

  public addFruit(newFruit: {}):Observable<FruitModel>{
    return this.http.post<FruitModel>(this.baseUrl + "api/fruit", newFruit).pipe(
      catchError(err => {
        const error = "Error creating new fruit.";
        this.router.navigate(['/error/', {error: error}]);
        throw new Error(err.message);
    })
    )
  }

  public updateFruit(updateFruit: {}, fruitId: string):Observable<FruitModel>{
    return this.http.put<FruitModel>(this.baseUrl + "api/fruit/" + fruitId, updateFruit).pipe(
      catchError(err => {
        const error = "Error to update a fruit.";
        this.router.navigate(['/error/', {error: error}]);
        throw new Error(err.message);
    })
    )
  }

  public deleteFruit(fruitId: string):Observable<void>{
    return this.http.delete<void>(this.baseUrl + "api/fruit/" + fruitId).pipe(
      catchError(err => {
        const error = "Error to delete a fruit.";
        this.router.navigate(['/error/', {error: error}]);
        throw new Error(err.message);
      })
    );
  }
}
