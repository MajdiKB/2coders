<?php

namespace Database\Seeders;

use App\Models\Fruits;
use Illuminate\Database\Seeder;

class FruitsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Fruits::truncate();

        $faker = \Faker\Factory::create();

        // And now, let's create a few articles in our database:
        
            Fruits::create([
                'name' => "Apple",
                'size' => "Small",
                'color' => "Red",
            ]);
            Fruits::create([
                'name' => "Orange",
                'size' => "Small",
                'color' => "Orange",
            ]);
            Fruits::create([
                'name' => "Manga",
                'size' => "Medium",
                'color' => "Orange-Green-Red",
            ]);
            Fruits::create([
                'name' => "Watermelon",
                'size' => "Big",
                'color' => "Green-Black",
            ]);
        
    }
}
