<?php

namespace Database\Factories;

use App\Models\Fruits;


$factory->define(Fruits::class, function () {
    return [
        'name' => "apple",
        'size' => "small",
        'color' => "red",
    ];
});
