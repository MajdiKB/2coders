<?php

namespace App\Http\Controllers;

use App\Models\Fruits;
use Illuminate\Http\Request;

class FruitController extends Controller
{
    public function index()
    {
        return Fruits::all();
    }

    public function show(Fruits $fruit)
    {
        return $fruit;
    }

    public function store(Request $request)
    {
        $fruit = Fruits::create($request->all());
        return response()->json($fruit, 201);
    }

    public function update(Request $request, Fruits $fruit)
    {
        $fruit->update($request->all());

        return response()->json($fruit, 200);
    }

    public function delete(Fruits $fruit)
    {
        $fruit->delete();
        return response()->json(null, 204);
    }
}
