<?php

use App\Models\Fruits;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('fruits', 'FruitController@index');
Route::get('fruit/{fruit}', 'FruitController@show');
Route::post('fruit', 'FruitController@store');
Route::put('fruit/{fruit}', 'FruitController@update');
Route::delete('fruit/{fruit}', 'FruitController@delete');

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('fruits', function () {
    // If the Content-Type and Accept headers are set to 'application/json', 
    // this will return a JSON structure. This will be cleaned up later.
    return Fruits::all();
});

Route::get('fruit/{fruit}', function ($id) {
    return Fruits::find($id);
});

Route::post('fruit', function (Request $request) {
    $data = $request->all();
        return Fruits::create([
            'name' => $data['name'],
            'size' => $data['size'],
            'photo' => $data['photo'],
            'color' => $data['color'],
        ]);
});

Route::put('fruit/{fruit}', function (Request $request, $id) {
    $fruit = Fruits::findOrFail($id);
    $fruit->update($request->all());

    return $fruit;
});

Route::delete('fruit/{fruit}', function ($id) {
    Fruits::find($id)->delete();
    return response()->json(204);
});
