<?php

namespace Tests\Feature;

use App\Models\Fruits;
use Tests\TestCase;

class FruitUpdateTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testsFruitsAreUpdatedCorrectly()
    {

        $fruit = factory(Fruits::class)->create([
            'name' => 'apple',
            'color' => 'red',
            'size' => 'small',
            'photo' => 'urlPhoto',
        ]);

        $payload = [
            'name' => 'apple',
            'color' => 'red',
            'size' => 'small',
            'photo' => 'urlPhoto',
        ];

        $response = $this->json('PUT', '/api/fruit/' . $fruit->id, $payload)
            ->assertStatus(200)
            ->assertJson([
                'id' => 1,
                'name' => 'apple',
                'color' => 'red',
                'size' => 'small',
                'photo' => 'urlPhoto',
            ]);
    }
}
